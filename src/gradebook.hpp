#include <vector>
#include <fstream>
#include "grade.cpp"

using namespace std;

class GradeBook {
    public:
        vector<Grade> grades;

        GradeBook();
        GradeBook(vector<Grade> grades);
        double gpa() const;

        int serialize() const;
        static vector<Grade> deserialize() {
            ifstream prefs;
            prefs.open("gradebook.txt");
            string line;
            vector<Grade> grades;

            while (getline(prefs, line)) {
                const char * grade_arr = line.c_str();

                char letter = grade_arr[0];
                char symbol = grade_arr[1];
                string type;
                switch (grade_arr[2]) {
                    case 'A':
                        type = "Accelerated";
                        break;
                    case 'C':
                        type = "CP";
                        break;
                    case 'H':
                        type = "Honors";
                        break;
                }

                grades.push_back(Grade(letter, symbol, type));
            }

            prefs.close();
            return grades;
        }

        void edit();
};