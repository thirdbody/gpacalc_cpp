#include <fstream>
#include <string>
#include <curl/curl.h>

using namespace std;

void getLatestVersion();
int checkVersion(string currentVersion);

int checkVersion(string currentVersion) {
    getLatestVersion();
    ifstream latest("latestversion.txt");

    if (!latest) {
        return -1;
    } else {
        string latestVersion;
        latest >> latestVersion;
        if (latestVersion != currentVersion) {
            latest.close();
            remove("latestversion.txt");
            return 1;
        } else {
            latest.close();
            remove("latestversion.txt");
            return 0;
        }
    }
}

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

void getLatestVersion() {
    CURL *curl;
    FILE *fp;
    CURLcode res;
    char *url = "https://myl0g.keybase.pub/gpacalc_cpp/VERSION";
    char outfilename[FILENAME_MAX] = "latestversion.txt";
    curl = curl_easy_init();
    if (curl) {
        fp = fopen(outfilename,"wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        /* always cleanup */
        curl_easy_cleanup(curl);
        fclose(fp);
    }
}
