#include "gradebook.hpp"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

GradeBook::GradeBook() {}
GradeBook::GradeBook(vector<Grade> grades) {
    this->grades = grades;
}

double GradeBook::gpa() const {
    double undivided = 0.0;

    for (int i = 0; i < grades.size(); i++) {
        double gradeWorth = grades.at(i).letter;
        gradeWorth += grades.at(i).symbol; // Apply symbol
        gradeWorth *= grades.at(i).type; // Apply weight
        undivided += gradeWorth;
    }
    return undivided / grades.size(); // The *a*verage in GP*A*
}

int GradeBook::serialize() const {

    ofstream prefs("gradebook.txt");
    if (!prefs) {
        return -1;
    }

    for (int i = 0; i < grades.size(); i++) {
        prefs << grades.at(i).serialize() << endl;
    }

    prefs.close();
    return 0;
}

void GradeBook::edit() {
    while (true) {

        for (int i = 0; i < grades.size(); i++) {

            cout << to_string(i) + ") ";

            if (grades.at(i).type == 1.00) {
                cout << "CP: ";
            } else if (grades.at(i).type == 1.15) {
                cout << "Honors: ";
            } else {
                cout << "Accelerated: ";
            }

            cout << grades.at(i).letter_rep;
            if (grades.at(i).symbol_rep != "0") {
                cout << grades.at(i).symbol_rep;
            }
            cout << endl;
        }

        cout << "Enter the number of the grade you'd like to edit, or 'q' to stop editing: ";
        char response;
        cin >> response;

        if (response == 'q' || response == 'Q') {
            this->serialize();
            return;
        } else {
            cout << "Enter in C for CP, H for Honors, or A for Accelerated: ";
            char type_ch;
            cin >> type_ch;
            
            string type;
            if (type_ch == 'A') {
                type = "Accelerated";
            } else if (type_ch == 'C') {
                type = "CP";
            } else {
                type = "Honors";
            }
        
            cout << "Enter in your grade: ";
            string grade;
            cin >> grade;

            const char * gradeArray = grade.c_str(); // Like substring but better
            char letter = toupper(grade[0]); // Doesn't change anything if letter is already uppercase

            char symbol;
            if (grade[1] != '+' && grade[1] != '-') {
                symbol = '0'; // Will register as 0.00 when calculating GPA
            } else {
                symbol = grade[1];
            }


            int index = response - '0';
            Grade newGrade(letter, symbol, type);

            grades.at(index) = newGrade;
        }


    }

}