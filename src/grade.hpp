#include <string>
using namespace std;

class Grade {
    public:
        double letter;
        double symbol;
        double type; // 1.00 = CP, 1.15 = Honors, 1.30 = Accelerated

        // For use in serialize():
        string letter_rep;
        string symbol_rep;
        string type_rep; // C for CP; H for Honors, A for Accel.

        Grade(char letter, char symbol, string type); // chars are converted to representative values
        Grade(string letter, double symbol, double type);
        string serialize() const;
};
