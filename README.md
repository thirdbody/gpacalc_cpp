# NBPS GPA Calculator

*Developer note: CI/CD is provided by GitLab, and can be seen in the side panel of the repo site.*

Calculates your GPA using the NBPS weighting system.

My source for the weighting system can be found [here](https://sites.google.com/a/mynbps.org/nbps-overview-course-handbook-description/gpa-and-weighting).

## Download

[Here](https://gitlab.com/Myl0g/gpacalc_cpp/tags) you go.

## Demo
[![asciicast](https://asciinema.org/a/IJBxKTRCUCZGnKmmrhS1rFttP.png)](https://asciinema.org/a/IJBxKTRCUCZGnKmmrhS1rFttP)

## Problems?

If you've got any issues, feel free to contact me at [myl0gcontact@gmail.com](mailto:myl0gcontact@gmail.com) or open an [issue](https://github.com/Myl0g/gpacalc_cpp/issues).

## Building

### nix-Like Systems

```
cmake CMakeLists.txt
make
```

Do with the executable whatever you'd like (note: make install isn't supported).

### Windows

You'll need the [Visual Studio](https://www.visualstudio.com/) IDE and CMake. Make sure you install the C++ components for Visual Studio.

Open this repo in VS. CMake should automatically generate the needed files. Then, use the green play button to build/change targets.
